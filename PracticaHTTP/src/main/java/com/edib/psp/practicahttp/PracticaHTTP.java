package com.edib.psp.practicahttp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import static org.apache.http.HttpHeaders.USER_AGENT;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

public class PracticaHTTP {

    // HTTP GET request
    public String sendGet(Map<String, String> variables) throws Exception {

        //Variables.
        StringBuilder laquery = new StringBuilder();
        String q = "";

        //fragmento del tutorial linea 16-21.
        laquery.append("?");
        if (variables != null && variables.keySet().size() > 0) {
            for (String key : variables.keySet()) {
                laquery.append(key).append("=").append(variables.get(key)).append("&");
            }

            q = laquery.toString().substring(0, laquery.length() - 1);
        }

        String url = "http://httpbin.org/get" + q;

        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);

        //Código tutorial.
        request.addHeader("User-Agent", USER_AGENT);

        HttpResponse response = client.execute(request);

        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : "
                + response.getStatusLine().getStatusCode());
        //BufferredReader
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        return result.toString();
    }

    // HTTP POST request
    public String sendPost(Map<String, String> parametros) throws Exception {

        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();

        if (parametros != null && parametros.keySet().size() > 0) {
            for (String key : parametros.keySet()) {
                urlParameters.add(new BasicNameValuePair(key, parametros.get(key)));
            }
        }

        String url = "http://httpbin.org/post";

        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(url);

        // add header
        post.setHeader("User-Agent", USER_AGENT);
        post.setEntity(new UrlEncodedFormEntity(urlParameters));

        HttpResponse response = client.execute(post);
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + post.getEntity());
        System.out.println("Response Code : "
                + response.getStatusLine().getStatusCode());

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        return result.toString();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Restaurante2;

/**
 *
 * @author Jordi
 */
public class Restaurante implements Runnable {

    Cliente cl = new Cliente();
    Chef ch = new Chef();
    Camarero m = new Camarero();

    public synchronized void makeOrder() throws InterruptedException {
        notifyAll();
        cl.makeOrder();
        wait();

    }

    public synchronized void makeServing() throws InterruptedException {

        notifyAll();
        wait();
        System.out.println("Servido" + Thread.currentThread());

    }

    public synchronized void makeFood() throws InterruptedException {
        notifyAll();
        ch.makeFood();
        Thread.sleep(1000);
        wait();
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i < 10; i++) {
                makeOrder();
                makeServing();
                makeFood();
            }
            
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}

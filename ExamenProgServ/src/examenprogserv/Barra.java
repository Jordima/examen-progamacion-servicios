/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenprogserv;

/**
 *
 * @author alumno
 */
public class Barra {

    private final int MAX_SIZE = 1000;

    private Object[] list = new Object[MAX_SIZE];
    private int head = 0;
    private int tail = 0;

    public Object pop() throws Exception {

        if (head == tail) {
            throw new Exception("No hay elementos en la cola");
        }

        Object ob = list[head];
        head++;
        return ob;

    }

    public void push(Object ob) throws Exception {
        if (tail == MAX_SIZE) {
            throw new Exception("La cola esta llena");
        }
        list[tail] = ob;
        tail++;
    }

}

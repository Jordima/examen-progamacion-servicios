/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenprogserv;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jordi
 */
public class Camarero implements Runnable {

    private Barra barra;

    public Camarero(Barra barra) {
        this.barra = barra;
    }

    public void run() {

        for (int i = 0; i < 10; i++) {
            try {
                barra.pop();
                System.out.println(Thread.currentThread() + "ha cogido plato");

            } catch (Exception ex) {
                Logger.getLogger(Camarero.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }
}
